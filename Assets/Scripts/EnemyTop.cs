using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTop : MonoBehaviour {

	private Transform playerTransform;
	private Vector3 newPos;

	public Transform ballPosition;

	public float speed;
	private float move;
	public float limit;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
		if (ballPosition.position.x > transform.position.x) {
			move = 1;
		} else {
			move = -1;
		}
		if (ballPosition.position.y <= 0) {
			move = 0;
		}
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(move, 0, 0);

		if (transform.position.x < -limit) {
			transform.position = new Vector3 (-limit,transform.position.y, transform.position.z);
		}else if(transform.position.x > limit) {
			transform.position = new Vector3 (limit,transform.position.y, transform.position.z);
		}
	}
}

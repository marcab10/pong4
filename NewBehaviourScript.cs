﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTop : MonoBehaviour {

	private Transform playerTransform;
	private Vector3 newPos;

	public Transform ballPosition;

	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
		if (ballPosition.position.y > transform.position.x) {
			move = 1;
		} else {
			move = -1;
		}
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(move, 0, 0);

		if (transform.position.x < -4.4f) {
			transform.position = new Vector3 (-4.4f, transform.position.y, transform.position.z);
		}else if(transform.position.x > -4.4f) {
			transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		}
	}
}
